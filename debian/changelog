ruby-solve (4.0.4-1) unstable; urgency=medium

  * Team upload.
  * Update watch file redirecting
  * d/control: Fix ruby dependencies
  * New upstream version 4.0.4
  * Rediff patches: 
    - 0001-Do-not-require-bundler-and-remove-spork.
    - 0003-Disable-gecode-solver.
    - 0003-Temporary-disable-test-which-depend-on-gecode. 
  * Bump Standards-Version to 4.6.2 (no changes)

 -- Adne Moretti Moreira <morettiadne1@gmail.com>  Wed, 08 Nov 2023 22:36:33 -0300

ruby-solve (4.0.0-2) UNRELEASED; urgency=medium

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 07:44:41 +0530

ruby-solve (4.0.0-1) unstable; urgency=medium

  * New upstream version
  * Point Vcs-* to salsa.d.o
  * Use https in watch file
  * Set dh compat level to 11
  * Bump Standards-Version to 4.1.3 (no changes)
  * Update dependencies versions
  * Refresh patch
  * Temporary remove gecode support as it's broken:
    - Disable tests which depend on gecode
    - Remove build dependency on ruby-dep-selector
    - Add Breaks for berkshelf (<< 6.3.1-1~)

 -- Hleb Valoshka <375gnu@gmail.com>  Fri, 23 Mar 2018 19:00:13 +0300

ruby-solve (3.0.1-1) unstable; urgency=medium

  [ Cédric Boutillier ]
  * Use https:// in Vcs-* fields
  * Run wrap-and-sort on packaging files

  [ Hleb Valoshka ]
  * Imported Upstream version 3.0.0
  * d/control:
    - Bump Standards-Version to 3.9.8 (no changes)
    - Set minimal ruby-molinillo version to 0.4
  * Drop patch for Molinillo 0.4 (supported by upstream)
  * Check gem dependencies during build

 -- Hleb Valoshka <375gnu@gmail.com>  Mon, 08 Aug 2016 14:21:44 +0300

ruby-solve (2.0.2-1) unstable; urgency=medium

  * Team upload

  [ Christian Hofstaedtler ]
  * Add patch for Molinillo 0.4

  [ Sebastien Badia ]
  * Imported Upstream version (Closes: #816359)
  * Bump compat. version to 9
  * d/control:
      Update Vcs-* fields (use https everywhere)
      Bump Standards-Version to 3.9.7 (no changes)
  * Update Debian packaging using dh-make-ruby
  * d/patches:
      Remove spork usage (ROM)
      Remove Change-Version-to-Semverse-Version patch (applied upstream)

 -- Sebastien Badia <seb@sebian.fr>  Thu, 03 Mar 2016 15:27:17 -0300

ruby-solve (2.0.1-2) unstable; urgency=medium

  * Run tests for Gecode solver during package build.
  * Bump ruby-dep-selector version to require proper one.

 -- Hleb Valoshka <375gnu@gmail.com>  Fri, 24 Jul 2015 18:38:20 +0300

ruby-solve (2.0.1-1) unstable; urgency=medium

  * Initial release.

 -- Hleb Valoshka <375gnu@gmail.com>  Fri, 26 Jun 2015 00:36:34 +0300
